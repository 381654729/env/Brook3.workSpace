# h
## 使用说明
1. 配置环境变量(linux为例)
    ```sh
    # 设置工作目录
    export H=/h
    # 终端管理器
    export TERMINAL_MANAGER=tmux
    # 设置文件管理器
    export FILE_MANAGER=ranger
    # 设置git管理器
    export GIT_MANAGER=lazygit
    # 设置编辑器
    export EDITOR=vim
    ```


## 目录介绍
```sh
.
├── app
├── code
├── data
└── README.md
```

`app`为配置集合，`code`为代码集合，`data`为数据集合。


## h e
> 目前使用vim


## h w
> 目前使用的docker-compose
