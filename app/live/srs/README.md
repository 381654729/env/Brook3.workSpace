# srs
[github](https://github.com/ossrs/srs)

## 使用方式
1. 容器启动后，可在网页端查看`http://127.0.0.1:83/`，获取到推流地址：`rtmp://127.0.0.1/live/livestream`
2. 在`OBS`中设置直播，服务为`自定义`，服务器为上步拿到的推流地址，推流码为`空`
3. 通过python脚本中的`FFmpeg`将流推送到各平台
4. 直播内容在`OBS`中调整即可

## 常见问题
### 反向代理
`http://127.0.0.1:83/` 尝试使用`srs.localhost`反向代理，但是生成的`rtmp://srs.localhost/live/livestream`推送不了，故放弃反向代理
