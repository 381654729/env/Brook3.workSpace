vim9script

g:mapleader = "\<Space>"

# module
import "~/.config/vim/vimrc.d/basic.vim"
source ~/.config/vim/vimrc.d/functions.vim
source ~/.config/vim/vimrc.d/plugin.vim
source ~/.config/vim/vimrc.d/snippets.vim
source ~/.config/vim/vimrc.d/keymap.vim

