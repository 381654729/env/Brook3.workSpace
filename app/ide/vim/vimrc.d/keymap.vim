"""""""""""""""""""""""""""""""""""""""""
" 自定义快捷键
"""""""""""""""""""""""""""""""""""""""""

" 普通模式(Nomal Mode)下快捷键
"""""""""""""""""""""""""""""""""""""""""
" 命令行模式(Command Line Mode)下快捷键
"""""""""""""""""""""""""""""""""""""""""
cnoremap <C-a> <Home>
cnoremap <C-e> <End>
" insert模式(Nomal Mode)下快捷键
"""""""""""""""""""""""""""""""""""""""""
inoremap <C-a> <home>
inoremap <C-e> <end>
inoremap <C-h> <left>
inoremap <C-j> <down>
inoremap <C-k> <up>
inoremap <C-l> <right>



" 多文件编辑
"""""""""""""""""""""""""""""""""""""""""
" window
nmap <Leader>wh <C-W>h
nmap <Leader>wj <C-W>j
nmap <Leader>wk <C-W>k
nmap <Leader>wl <C-W>l

" buffer
nmap <leader>bn :bn<cr>
nmap <leader>bp :bp<cr>
nmap <leader>bd :bd<cr>
nmap <leader>bl :ls<cr>



" config
"""""""""""""""""""""""""""""""""""""""""
" Fast reloading of the .vimrc
nmap <leader>r :source $MYVIMRC<cr>

