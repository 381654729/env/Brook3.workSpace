" 代码检测
"""""""""""""""""""""""""""""""""""""""""
Plug 'w0rp/ale'

" set to 1, the nvim will auto close current preview window when change
" from markdown buffer to another buffer
" default: 1
let g:mkdp_auto_close = 1

" automated bullet lists
Plug 'dkarter/bullets.vim'

""" Compile function
"""""""""""""""""
noremap <silent> <leader>r :call CompileRunGcc()<CR>
function! CompileRunGcc()
    exec "w"
    if &filetype == 'markdown'
        exec "MarkdownPreviewToggle"
    elseif &filetype == 'c'
        exec "!gcc % -o %<"
        exec "!time ./%<"
    elseif &filetype == 'cpp'
        set splitbelow
        exec "!g++ -std=c++11 % -Wall -o %<"
        :sp
        :res -15
        :term ./%<
    elseif &filetype == 'java'
        exec "!javac %"
        exec "!time java %<"
    elseif &filetype == 'sh'
        :!time bash %
    elseif &filetype == 'python'
        set splitbelow
        :sp
        :term python3 %
    elseif &filetype == 'html'
        silent! exec "!".g:mkdp_browser." % &"
    elseif &filetype == 'tex'
        silent! exec "VimtexStop"
        silent! exec "VimtexCompile"
    elseif &filetype == 'dart'
        exec "CocCommand flutter.run -d ".g:flutter_default_device." ".g:flutter_run_args
        silent! exec "CocCommand flutter.dev.openDevLog"
    elseif &filetype == 'javascript'
        set splitbelow
        :sp
        :term export DEBUG="INFO,ERROR,WARNING"; node --trace-warnings .
    elseif &filetype == 'go'
        set splitbelow
        :sp
        :term go run .
    endif
endfunc

Plug 'skywind3000/asynctasks.vim'
Plug 'skywind3000/asyncrun.vim'
let g:asyncrun_open = 6

source ~/.config/vim/vimrc.d/plugin.d/dap.d/go.vim
source ~/.config/vim/vimrc.d/plugin.d/dap.d/html.vim
source ~/.config/vim/vimrc.d/plugin.d/dap.d/md.vim
source ~/.config/vim/vimrc.d/plugin.d/dap.d/sql.vim
source ~/.config/vim/vimrc.d/plugin.d/dap.d/git.vim

