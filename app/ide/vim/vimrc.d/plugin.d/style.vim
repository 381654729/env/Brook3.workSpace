""" style
"""""""""""""""""""""""""""""""""""""""""""""""""""
" 缩进线
Plug 'Yggdroot/indentLine'
let g:indent_guides_guide_size = 1 " 指定对齐线的尺寸
let g:indent_guides_start_level = 2 " 从第二层开始可视化显示缩进
let g:indentLine_enabled = 0 " 禁用json隐藏引号

""" 配色
"""""""""""""""""""""""""""""""""""""""""
" Plug 'altercation/vim-colors-solarized'
" Plug 'tomasr/molokai'
Plug 'morhetz/gruvbox'

" Status line
"""""""""""""""""""""""""""""""""""""""""
Plug 'itchyny/lightline.vim'
Plug 'edkolev/tmuxline.vim'
set laststatus=2
let g:lightline = {
    \ 'colorscheme': 'wombat',
    \ 'active': {
    \   'left': [ [ 'mode', 'paste' ],
    \             [ 'readonly', 'filename', 'modified' ] ]
    \ },
    \ 'component': {
    \   'lineinfo': '%3l:%-2v%<',
    \ },
    \ }
