"""""""""""""""""""""""""""""""""""""""""
" 自定义的函数
"""""""""""""""""""""""""""""""""""""""""
" 获取vi 类型
function! GetViType()
    let type_file = split($MYVIMRC, '/')[-1]
    if type_file == '.vimrc'
        return 'vim'
    elseif type_file == 'init.vim'
        return 'nvim'
    else
        return ''
    endif
endfunc


" 相对绝对行号转换
function! NumberToggle()
  if(&relativenumber == 1)
    set norelativenumber number
  else
    set relativenumber
  endif
endfunc
map <leader>fn :call NumberToggle()<cr>


" 突出显示当前行列转换
function! CusorCulLineToggle()
  if(&cursorcolumn == 1)
    set nocursorcolumn
    set nocursorline
  else
    set cursorcolumn
    set cursorline
  endif
endfunc
map <leader>fc :call CusorCulLineToggle()<cr>



"自动回到档案最后读取位置
if has("autocmd")
    autocmd BufRead *.txt set tw=78
    autocmd BufReadPost *
    \ if line("'\"") > 0 && line ("'\"") <= line("$") |
    \   exe "normal g'\"" |
    \ endif
endif



" 快速记日志（php）
map <Leader>l :call ErrorLog()<cr>
function! ErrorLog()
	let errorLogLine=line(".") + 1
    call append(line("."),"error_log('".expand("%:t").":".errorLogLine." --- .log '.date('Y-m-d H:i:s').',Brook3:'.var_export($Brook3,1).\"\\n\",3,ROOT_DIR.'/log/Brook3.txt');")
endf



" 快速添加版权和作者信息
map <Leader>ft :call TitleDet()<cr>
function! AddTitle()
    call append(0,"\#!/usr/bin/env bash")
    call append(1,"# ******************************************************")
    call append(2,"# Author       : Brook3")
    call append(3,"# Last modified: ".strftime("%Y-%m-%d %H:%M"))
    call append(4,"# Email        : Brook3@foxmail.com")
    call append(5,"# Filename     : ".expand("%:t"))
    call append(6,"# Description  : ")
    call append(7,"# ******************************************************")
    echohl WarningMsg | echo "Successful in adding copyright." | echohl None
endf
 
function! UpdateTitle()
     normal m'
     execute '/# Last modified/s@:.*$@\=strftime(":\t%Y-%m-%d %H:%M")@'
     normal ''
     normal mk
     execute '/# Filename/s@:.*$@\=":\t".expand("%:t")@'
     execute "noh"
     normal 'k
     echohl WarningMsg | echo "Successful in updating the copyright." | echohl None
endfunction

function! TitleDet()
    let n=1
    while n < 10
        let line = getline(n)
        if line =~ '^\#\s*\S*Last\smodified\S*.*$'
            call UpdateTitle()
            return
        endif
        let n = n + 1
    endwhile
    call AddTitle()
endfunction
