#!/bin/bash

minikube stop
minikube delete --all

source ${H}/app/containers/k8s/minikube/start.sh
