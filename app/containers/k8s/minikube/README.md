## start
详情见start脚本

## addons
minikube addons enable ingress

## macos
### driver=docker
macos中宿主机ping不通docker容器，导致k8s中使用受阻

### driver=hyperkit
如果你安装了docker desktop，则默认已安装。但是 minikube 中无法在macos m1中使用这个driver

### driver=qemu
支持m1！！！

[官网](https://minikube.sigs.k8s.io/docs/drivers/qemu/) 介绍：

需下载 socket_vmnet ，要不然宿主机还是ping不通容器
