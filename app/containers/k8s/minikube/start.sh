#!/bin/sh

OS="`uname`"
MINIKUBE_START_COMMAND='--driver docker'
case $OS in
  'Linux')
    OS='Linux'
    ;;
  'Darwin') 
    OS='Mac'
    MINIKUBE_START_COMMAND='
        --driver qemu
        --network socket_vmnet
    '
    ;;
  'WindowsNT')
    OS='Windows'
    ;;
  'FreeBSD')
    OS='FreeBSD'
    ;;
  'SunOS')
    OS='Solaris'
    ;;
  'AIX') ;;
  *) ;;
esac

minikube start --image-mirror-country=cn ${MINIKUBE_START_COMMAND}
