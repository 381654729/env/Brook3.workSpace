# blender

[官网下载](https://www.blender.org/download/)

[官方参考手册](https://docs.blender.org/manual/zh-hans/3.6/index.html)

## 配置文件

## 常用操作
> macos

* 旋转：双指
* 位移：shift + 双指
* 缩放：ctrl + 双指

* 移动
    * x轴移动 gx： 单指

* 添加： A
* 赋值： D

### 物体模式
* `~`切换视图
* `e`挤出
* `option + z`透视模式
