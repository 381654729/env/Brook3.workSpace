# wacom
[官网](https://www.wacom.com/zh-cn)

## 快捷键
### 触控笔
* 下边的键位，设置为鼠标中键单击
* 上边的键位，设置为鼠标右键单击

这样按着`下键`为旋转操作；配合`Ctrl键`为缩放操作；配合`shift键`为位移操作。

## 购买福利
```
Wacom-绘梦营课程
链接： https://pan.baidu.com/s/1kaJLZNNvBeAwGIIZYwyhHQ
提取码：kf6L

Wacom学院课程  https://community.wacom.com.cn/gyxy.html
绘画软件，笔刷
链接：https://pan.baidu.com/s/1Sb_kDSAC-ViXDLKhT-kIUg?pwd=22sz#list/path=%2F
提取码：22sz
```
