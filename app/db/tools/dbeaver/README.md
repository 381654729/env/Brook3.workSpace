# dbeaver
[官网](https://dbeaver.io/)

## 驱动配置
首选项-》连接-》驱动-》Maven-》添加地址并调整至第一位：
`https://maven.aliyun.com/nexus/content/groups/public/`



# 连接问题（暂未解决）
在使用 sqlite3 时，发现问题：

1. 当在dbevear中修改数据数据后，在dbevear中查看是生效的。
2. 查看事务提交，也没有未提交的事务。
3. 保持dbevear打开状态，这时用代码或者其他工具查看，dbevear修改的数据不能正常获取到
4. 当dbevear关闭后，修改的数据在其他地方就能正常获取

> 之前在公司，同事使用mysql时，好像也遇到了类似的问题
